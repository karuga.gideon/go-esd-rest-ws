package main

import (
	"go-esd-ws/app"
	"go-esd-ws/config"
)

func main() {
	config := config.GetConfig()

	app := &app.App{}
	app.Initialize(config)
	app.Run(":8098")
}
