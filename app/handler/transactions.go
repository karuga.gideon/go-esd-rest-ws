package handler

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"go-esd-ws/app/model"
)

func GetAllInvoices(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	invoices := []model.Invoice{}
	db.Find(&invoices)
	respondJSON(w, http.StatusOK, invoices)
}

func CreateInvoice(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	invoice := model.Invoice{}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&invoice); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&invoice).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, invoice)
}

func GetInvoice(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	invoice_number := vars["invoice_number"]
	invoice := getInvoiceOr404(db, invoice_number, w, r)
	if invoice == nil {
		return
	}
	respondJSON(w, http.StatusOK, invoice)
}

func UpdateInvoice(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	invoice_number := vars["invoice_number"]
	invoice := getInvoiceOr404(db, invoice_number, w, r)
	if invoice == nil {
		return
	}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&invoice); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&invoice).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, invoice)
}

func DeleteInvoice(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	invoice_number := vars["invoice_number"]
	invoice := getInvoiceOr404(db, invoice_number, w, r)
	if invoice == nil {
		return
	}
	if err := db.Delete(&invoice).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusNoContent, nil)
}

func ArchiveInvoice(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	invoice_number := vars["invoice_number"]
	invoice := getInvoiceOr404(db, invoice_number, w, r)
	if invoice == nil {
		return
	}
	// invoice.Archive()
	if err := db.Save(&invoice).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, invoice)
}

func RestoreInvoice(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	invoice_number := vars["invoice_number"]
	invoice := getInvoiceOr404(db, invoice_number, w, r)
	if invoice == nil {
		return
	}
	// invoice.Restore()
	if err := db.Save(&invoice).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, invoice)
}

// getInvoiceOr404 gets a invoice instance if exists, or respond the 404 error otherwise
func getInvoiceOr404(db *gorm.DB, invoice_number string, w http.ResponseWriter, r *http.Request) *model.Invoice {
	invoice := model.Invoice{}
	if err := db.First(&invoice, model.Invoice{InvoiceNumber: invoice_number}).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &invoice
}
