package model

import (
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)



// Users table / model
type User struct {
  Id      int `json:"id"`
  Email    string `json:"email"`
	Firstname  string `json:"firstname"`
  Lastname  string `json:"lastname"`
	Msisdn  string `json:"msisdn"`
  Password  string `json:"password"`
  Apikey  string `json:"apikey"`
  ExpiryTime    string `json:"expiry_time"`
  CreationTime    time.Time `json:"creation_time"`
  InTrash    string `json:"in_trash"`
	ValidationCode    string `json:"validation_code"`
}


// Invoice table / model
type Invoice struct {
	Id      int `json:"id"`
	DateCreated    time.Time `json:"date_created"`
  InvoiceDate   string `json:"invoice_date"`
  InvoiceNumber  string `json:"invoice_number"`
  Vat  string `json:"vat"`
  GrandTotal    string `json:"grand_total"`
  Signature    string `json:"signature"`
}


// DBMigrate will create and migrate the tables, and then make the some relationships if necessary
func DBMigrate(db *gorm.DB) *gorm.DB {
	// db.AutoMigrate(&Organization{}, &User{}, &Transaction{})
	// db.Model(&User{}).AddForeignKey("org_id", "organizations(id)", "CASCADE", "CASCADE")
	return db
}
