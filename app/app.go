package app

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"go-esd-ws/app/handler"
	"go-esd-ws/app/model"
	"go-esd-ws/config"
)

// App has router and db instances
type App struct {
	Router *mux.Router
	DB     *gorm.DB
}

// Initialize initializes the app with predefined configuration
func (a *App) Initialize(config *config.Config) {
	dbURI := fmt.Sprintf("%s:%s@/%s?charset=%s&parseTime=True",
		config.DB.Username,
		config.DB.Password,
		config.DB.Name,
		config.DB.Charset)

	db, err := gorm.Open(config.DB.Dialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}

	a.DB = model.DBMigrate(db)
	a.Router = mux.NewRouter()
	a.setRouters()
}

// setRouters sets the all required routers
func (a *App) setRouters() {
	// Routing for handling the users
	a.Get("/users", a.GetAllUsers)
	a.Post("/users", a.CreateUser)
	a.Get("/users/{title}", a.GetUser)
	a.Put("/users/{title}", a.UpdateUser)
	a.Delete("/users/{title}", a.DeleteUser)
	a.Put("/users/{title}/archive", a.ArchiveUser)
	a.Delete("/users/{title}/archive", a.RestoreUser)

	// Routing for handling the invoices
	a.Get("/invoices", a.GetAllInvoices)
	a.Get("/users/{title}/invoices", a.GetAllInvoices)
	a.Post("/users/{title}/invoices", a.CreateInvoice)
	a.Get("/users/{title}/invoices/{id:[0-9]+}", a.GetInvoice)
	a.Put("/users/{title}/invoices/{id:[0-9]+}", a.UpdateInvoice)
	a.Delete("/users/{title}/invoices/{id:[0-9]+}", a.DeleteInvoice)
	a.Put("/users/{title}/invoices/{id:[0-9]+}/complete", a.CompleteInvoice)
	a.Delete("/users/{title}/invoices/{id:[0-9]+}/complete", a.UndoInvoice)
}

// Get wraps the router for GET method
func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

// Post wraps the router for POST method
func (a *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

// Put wraps the router for PUT method
func (a *App) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")
}

// Delete wraps the router for DELETE method
func (a *App) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")
}

/*
** Users Handlers
 */
func (a *App) GetAllUsers(w http.ResponseWriter, r *http.Request) {
	handler.GetAllUsers(a.DB, w, r)
}

func (a *App) CreateUser(w http.ResponseWriter, r *http.Request) {
	handler.CreateUser(a.DB, w, r)
}

func (a *App) GetUser(w http.ResponseWriter, r *http.Request) {
	handler.GetUser(a.DB, w, r)
}

func (a *App) UpdateUser(w http.ResponseWriter, r *http.Request) {
	handler.UpdateUser(a.DB, w, r)
}

func (a *App) DeleteUser(w http.ResponseWriter, r *http.Request) {
	handler.DeleteUser(a.DB, w, r)
}

func (a *App) ArchiveUser(w http.ResponseWriter, r *http.Request) {
	handler.ArchiveUser(a.DB, w, r)
}

func (a *App) RestoreUser(w http.ResponseWriter, r *http.Request) {
	handler.RestoreUser(a.DB, w, r)
}

/*
** Invoices Handlers
 */
func (a *App) GetAllInvoices(w http.ResponseWriter, r *http.Request) {
	handler.GetAllInvoices(a.DB, w, r)
}

func (a *App) CreateInvoice(w http.ResponseWriter, r *http.Request) {
	handler.CreateInvoice(a.DB, w, r)
}

func (a *App) GetInvoice(w http.ResponseWriter, r *http.Request) {
	handler.GetInvoice(a.DB, w, r)
}

func (a *App) UpdateInvoice(w http.ResponseWriter, r *http.Request) {
	handler.UpdateInvoice(a.DB, w, r)
}

func (a *App) DeleteInvoice(w http.ResponseWriter, r *http.Request) {
	handler.DeleteInvoice(a.DB, w, r)
}

func (a *App) CompleteInvoice(w http.ResponseWriter, r *http.Request) {
	// handler.CompleteInvoice(a.DB, w, r)
	handler.UpdateInvoice(a.DB, w, r)
}

func (a *App) UndoInvoice(w http.ResponseWriter, r *http.Request) {
	// handler.UndoInvoice(a.DB, w, r)
	handler.UpdateInvoice(a.DB, w, r)
}

// Run the app on it's router
func (a *App) Run(host string) {
	log.Fatal(http.ListenAndServe(host, a.Router))
}
